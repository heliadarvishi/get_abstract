<?php
use App\Models\Payment;
function zibalRequest($mobile,$callback,$summary)
  {
      $parameters = array(
          "merchant"    => env('ZIBAL_MERCHANT_KEY'),//required
          "callbackUrl" => $callback,//required
          "amount"      => $summary->price,//required
          "mobile"      => $mobile,//optional
        //   "orderId"     => time(),//optional
      );

      $response = postToZibal('request', $parameters);

      if ($response->result == 100)
      {
          //save trackId into database if you want or do something else
          $startGateWayUrl = "https://gateway.zibal.ir/start/".$response->trackId."/direct";
        //   header('location: '.$startGateWayUrl);
        return $startGateWayUrl;
      }
      return $response;
  }

  function callback($array)
  {
      if($array['success']==1) {
          //start verfication
          $parameters = array(
              "merchant" => env('ZIBAL_MERCHANT_KEY'),//required
              "trackId"  => $array['trackId'],//required
          );
          $response = postToZibal('verify', $parameters);
          //insert to database
          Payment::store($response);

          if ($response->result == 100) {
            //   dd($response);
            return $response;
              //update database or something else
          } else {
              echo "result: " . $response->result . "<br>";
              echo "message: " . $response->message;
          }
      }else{
          echo "پرداخت با شکست مواجه شد.";
      }
  }

  function postToZibal($path, $parameters)
  {
      $url = 'https://gateway.zibal.ir/v1/'.$path;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($parameters));
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $response  = curl_exec($ch);
      curl_close($ch);
      return json_decode($response);
  }

