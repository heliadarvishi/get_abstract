<?php
use App\Models\Rate;
use App\Models\BookMark;
use App\Models\Like;
use App\Models\Channel;
use App\Models\User;

function rateCalculate($parent_id,$type)
{
    $rates = Rate::where([
            ['parent_id',$parent_id],
            ['type',$type]
        ])->get();
    $sum = 0;
    foreach($rates as $rate)
        $sum += $rate['rank'];
    if($rates->count() == 0)
        return 0;
    else
        return $sum/$rates->count();
}

function bookMarkCalculate($summary_id,$type)
{    
    $response = BookMark::where([
            ['parent_id',$summary_id],
            ['user_id',auth()->user()->id],
            ['type',$type]
        ])->get()->count();
    if($response != 0)
        return true;
    else
        return false;
}

function likeCalculate($summary_id)
{
    $response =  Like::where([['summary_id',$summary_id],['user_id',auth()->user()->id]])->get()->count();
    if($response != 0)
        return true;
    else
        return false;
}

function getTitleChannel($channel_id)
{
    return Channel::find($channel_id)->title;
}

function getLogoChannel($channel_id)
{
    return 'https://cdn.pielem.ir/uploads/images/channel_logo/'.Channel::find($channel_id)->logo;
}

function getAuthor($user_id)
{
    return User::find($user_id);
}