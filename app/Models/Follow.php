<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = ["user_id","parent_id","type"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeChannel($query)
    {
        return $query->where([
                ['type','2']
            ]);
    }

    public function scopeUser($query)
    {
        return $query->where([
                ['type','1']
            ]);
    }

    public static function store($parent,$type)
    {
        $user = auth()->user();
        $result = Follow::where([
                ['parent_id',$parent->id],
                ['user_id',$user->id]
            ])->first();
        if($result == null)
        {
            $follow = Follow::create([
                "user_id"   => $user->id,
                "parent_id" => $parent->id,
                "type"      => $type
            ]); 
            return $follow ? true : false;
        }
        else
        return $result->delete() ? true : false;
    }
}
