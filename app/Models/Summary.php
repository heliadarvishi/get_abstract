<?php

namespace App\Models;
use Verta;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\FileCollection;

class Summary extends Model
{
    protected $fillable =['user_id','title','channel_id','category_id','text'];

    public function file()
    {
        return $this->hasMany(File::class);
    }

    public function like()
    {
        return $this->hasMany(Like::class);
    }

    public function channel()
    {
        return $this->hasMany(Channel::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function bookmark()
    {
        return $this->hasMany(BookMark::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function store($data)
    {
        $user = auth()->user();
        $summary = Summary::firstOrCreate([
            'title'       => $data['title'],
            'category_id' => $data['category_id'],
            'channel_id'  => $data['channel_id'],
            'user_id'     => $user->id
        ]);

//        File::create([
//            'summary_id' => $summary->id,
//            'user_id'    => $user->id,
//
//        ]);
    }

    public function deleteSummary($summary)
    {
        $user = auth()->user();
        $file = $summary->file()->first();
        if(isset($file))
            $file->delete();
        if($summary->user_id == $user->id)
            $summary->delete();
        return response()->json(true,200);
    }

    public function updateSummary(Summary $summary,$data)
    {
        return Response()->json('true', 200);
    }

    public function showInfoSummaryHome($follows)
    {
        $user = auth()->user();
        //show home summary info
        $summaries = Summary::where([
            "channel_id",$follows[channel_id]
        ])->get();
        return $summaries;
    }

    public static function fullSummary($summary)
    {
        if(auth()->user() != null)
        return[
            "id"                   => $summary['id'],
            "title"                => $summary['title'],
            "year_publication"     => $summary['year_publication'],
            "price"                => $summary['price'],
            "publisher"            => $summary['publisher'],
            "cover"                => 'https://cdn.pielem.ir/uploads/images/summary_cover/'.$summary['cover'],
            "created_at"           => verta($summary['created_at'])->format('Y n j','1396 مهر 17'),
            "updataed_at"          => $summary['updataed_at'],
            "bookmark"             => bookMarkCalculate($summary['id'],'1'),
            "rate"                 => rateCalculate($summary['id'],'1'),
            "like"                 => likeCalculate($summary['id']),
            "url_original_article" => $summary['url_original_article'],
            "text"                 => $summary['text'],
            "autor"                => User::getAutor($summary->user()->where('id',$summary['user_id'])->first()),
            "file_video"           => new FileCollection($summary->file()->where('type','4')->get()),
            "file_pdf"             => new FileCollection($summary->file()->where('type','1')->get()),
            "file_audio"           => new FileCollection($summary->file()->where('type','3')->get()),
            "file_text"            => new FileCollection($summary->file()->where('type','2')->get()),
        ];
        else
        return[
            "id"                   => $summary['id'],
            "title"                => $summary['title'],
            "year_publication"     => $summary['year_publication'],
            "price"                => $summary['price'],
            "publisher"            => $summary['publisher'],
            "cover"                => 'https://cdn.pielem.ir/uploads/images/summary_cover/'.$summary['cover'],
            "created_at"           => verta($summary['created_at'])->format('Y n j','1396 مهر 17'),
            "updataed_at"          => $summary['updataed_at'],
            "rate"                 => rateCalculate($summary['id'],'1'),
            "url_original_article" => $summary['url_original_article'],
            "text"                 => $summary['text'],
            "autor"                => User::getAutor($summary->user()->where('id',$summary['user_id'])->first()),
            "file_video"           => new FileCollection($summary->file()->where('type','4')->get()),
            "file_pdf"             => new FileCollection($summary->file()->where('type','1')->get()),
            "file_audio"           => new FileCollection($summary->file()->where('type','3')->get()),
            "file_text"            => new FileCollection($summary->file()->where('type','2')->get()),
        ];
    }

    //public data
    public static function getPaublicSummary($paginate)
    {
        return Summary::where('status','1')->latest()->paginate($paginate);
    }
}
