<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    const TYPE_POST = 'post';
    const TYPE_PAGE = 'page';

    protected $table = 'blogs';
    protected $fillable = ['user_id','body','title','type'];

    protected $attributes = [
        'type' => self::TYPE_POST
    ];
}
