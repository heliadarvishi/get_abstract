<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function summary()
    {
        return $this->belongsTo(Summary::class);
    }
}
