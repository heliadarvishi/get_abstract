<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = ['title','logo'];

    public function follow()
    {
        return $this->hasMany(Follow::class);
    }

    public function followCategory()
    {
        return $this->hasMany(FollowCategory::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function createChannel($data)
    {
        return $data;
        $logo = $data['logo'];
        return $logo;
        $user = auth()->user();
        $channel = Channel::firstOrCreate([
            'title'=> $data['title'],
        ]);
    }

    public function deleteChannel($channel)
    {
        $user = auth()->user();
        $summaries = $channel->summary()->get();
        foreach($summaries as $summary)
        {
            $summary->update(["channel_id" => null]);
            $summary->save();
        }
        $channel->delete();
        return response()->json(true,200);
    }

    public function updateChannel($channel,$data)
    {
        return $channel;
    }

    //public data
    public static function getPaublicChannel($paginate)
    {
        return Channel::where('status','1')->latest()->paginate($paginate);
    }
}
