<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['user_id','summary_id'];

    public function summary()
    {
        return $this->belongsTo(Summary::class);
    }

    public static function likeAndDislike($summary)
    {
        $user = auth()->user();
        $result = Like::where([
                ['summary_id',$summary['id']],
                ['user_id',$user->id]
            ])->first();
        if($result == null)
        {
            $like = new Like;
            $like->create([
                "user_id"    => $user->id,
                "summary_id" => $summary['id']
            ]);
            return $like ? true : false;
        }
        else
        return $result->delete() ? true : false;
    }

    public static function countLike($summary)
    {
        return $summary->like()->get()->count();
    }
}
