<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BookMark;

class BookMark extends Model
{
    protected $fillable = ["user_id","parent_id","type"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function summary()
    {
        return $this->belongsTo(Summary::class);
    }

    public static function bookmarkAndUnBookmark($summary)
    {
        $user = auth()->user();
        $result = BookMark::where([
                ['parent_id',$summary['id']],
                ['type','1'],
                ['user_id',$user->id]
            ])->first();
        if($result == null)
        {
            $bookmark = new BookMark;
            $bookmark->create([
                "user_id"   => $user->id,
                "parent_id" => $summary['id'],
                "type"      => '1'
            ]);
            return $bookmark ? true : false;
        }
        else
        return $result->delete() ? true : false;
    }
}
