<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class FollowCategory extends Model
{
    protected $fillable = ["channel_id","user_id"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public static function store($categories)
    {
        $user = auth()->user();
        foreach($categories as $category_id)
        {
            $channels = Channel::where('category_id',$category_id)->select('id')->get();
            foreach($channels as $channel_id)
            {
                $follow_category = new FollowCategory;
                $follow_category->create([
                    "channel_id" => $channel_id['id'],
                    "user_id"    => $user->id
                ]);
            }
        }
        return true;
    }

    public static function recommended()
    {
        $user = auth()->user();
        return FollowCategory::join('users','follow_categories.user_id','users.id')
        ->join('channels','follow_categories.channel_id','channels.id')
        ->join('summaries','channels.id','summaries.channel_id')
        ->select('summaries.*')
        ->where([
                ['follow_categories.user_id',$user->id],
                ['summaries.status','1']
            ]);
    }

    public static function featuredChannels()
    {
        $user = auth()->user();
        return FollowCategory::join('channels','follow_categories.channel_id','channels.id')
        ->join('follows','follows.parent_id','channels.id')
        ->where([
                ['follow_categories.user_id',$user->id],
                ['follows.type','2'],
                ['channels.status','1']
            ])
        ->select('follows.parent_id',DB::raw('count(*) as total'))->groupBy('follows.parent_id')->orderby('total','DESC')->take(5);
    }

    public static function getAuthors()
    {
        $user = auth()->user();
        return FollowCategory::join('channels','follow_categories.channel_id','channels.id')
        ->join('summaries','channels.id','summaries.channel_id')
        ->select('summaries.*')->groupBy('summaries.user_id')
        ->where([
            ['follow_categories.user_id',$user->id],
            ['channels.status','1']
        ]);
    }

    public static function getPopularSummaries()
    {
        $user = auth()->user();
        return FollowCategory::join('channels','follow_categories.channel_id','channels.id')
        ->join('summaries','channels.id','summaries.channel_id')
        ->join('rates','summaries.id','rates.parent_id')
        ->where([
                    ['follow_categories.user_id',$user->id],
                    ['channels.status','1'],
                    ['rates.type','1'],
            ])
            ->select('rates.*')->orderby('rates.parent_id','DESC');
        // ->select('rates.parent_id',DB::raw('count(*) as total'))->groupBy('rates.parent_id')->orderby('total','DESC')->take(5);
    }
}
