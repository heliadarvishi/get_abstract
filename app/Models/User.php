<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use File;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'user_name',
        'mobile',
        'password',
        'avatar',
        'first_name',
        'last_name',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function summary()
    {
        return $this->hasMany(Summary::class);
    }

    public function bookmark()
    {
        return $this->hasMany(BookMark::class);
    }


    public function follow()
    {
        return $this->hasMany(Follow::class);
    }

    public function followCategory()
    {
        return $this->hasMany(FollowCategory::class);
    }


    public static function updateUserName($data)
    {
        $user = auth()->user();
        $user = $user -> update([
            "user_name" => $data['user_name']
        ]);
        return $user ? true : false;
    }

    public static function updateFullName($data)
    {
        $user = auth()->user();
        $user = $user -> update([
            "first_name" => $data['first_name'],
            "last_name"  => $data['last_name']
        ]);
        return $user ? true : false;
    }

    public static function updateAvatar($file)
    {
        $user = auth()->user();
        if ($file != null)
            $orginfilename = Input::file('avatar')->getClientOriginalName();
        if($user->avatar != null)
            File::delete($user->avatar);

        $destinationPath = '/images/profile_pic/';
        $fileName = $user->id.'_'.rand(1111,99999).$orginfilename;

        Storage::disk('ftp')->put($destinationPath.$fileName,fopen($file, 'r+'));
        $user->avatar = $fileName;

        if($user->save())
            return response()->json(true,'200');
    }

    public static function register($data)
    {
        $sms = Sms::where([
            ['mobile',$data['mobile']],
            ['code',$data['code']]
        ])->first();
        if(is_null($sms))
            return ["message" => "the code is invalid!"];

        $user = User::where('mobile',$data['mobile'])->first();
        if(is_null($user))
        {
            $user = new User;
            $user = $user->updateOrCreate([
                'mobile' => $data['mobile'],
                'type'   => '1'
            ]);
        }
        else
        {
            $user = new User;
            $user = $user->updateOrCreate([
                'mobile' => $data['mobile']
            ]);
        }
        $token = $user->createToken('user_token')->accessToken;
        return ["token" => $token];
    }

    public function showInfoAuthorlHome($follows)
    {
        $user = auth()->user();
        //show home author info
        $authors = User::where([
            "category_id",$follows[category_id]
        ])->get();
        return $authors;
    }

    public static function getAutor($user)
    {
        return [
            "first_name"  => $user['first_name'],
            "last_name"   => $user['last_name'],
            "profile_pic" => 'https://cdn.pielem.ir/uploads/images/profile_pic/'.$user['avatar'],
            "about"       => $user['about']
        ];
    }
}
