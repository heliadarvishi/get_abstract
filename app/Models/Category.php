<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function summary()
    {
        return $this->hasMany(Summary::class);
    }

    public function follow()
    {
        return $this->hasMany(Follow::class);
    }

    public function channel()
    {
        return $this->hasMany(Channel::class);
    }

}
