<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = ["user_id","parent_id","type","rank"];

    public static function store($parent,$type,$rank)
    {
        $user = auth()->user();
        $rate = Rate::create([
            "user_id"   => $user->id,
            "parent_id" => $parent->id,
            "type"      => $type,
            "rank"      => $rank
        ]); 
        return $rate ? true : false;
    }
}
