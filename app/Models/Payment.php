<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['user_id','price','ref_id','status','message'];
    public static function store($data)
    {
        $user = auth()->user();
        $payment = new Payment();
        $payment->create([
            'user_id' => $user->id,
            'price'   => $data->amount,
            'ref_id'  => $data->refNumber,
            'status'  => $data->status,
            'message' => $data->message
        ]);
        return true;
    }
}
