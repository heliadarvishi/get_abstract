<?php

namespace App\Http\Middleware;

use Closure;

class App_Key
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('X_API_TOKEN') == env('X_API_TOKEN'))
            return $next($request);
        return response()->json([
            'message' => 'دسترسی غیر مجاز',
            'code' => 403
        ]);
    }
}
