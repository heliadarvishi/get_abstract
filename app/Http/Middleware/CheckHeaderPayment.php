<?php

namespace App\Http\Middleware;

use Closure;

class CheckHeaderPayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(isset($request->X_API_TOKEN) && isset($request->authorization))
        {
            $request->headers->set('X_API_TOKEN',$request->X_API_TOKEN);
            $request->headers->set('Authorization',$request->authorization);
            $request->headers->set('Content-Type','application/json');
            $request->headers->set('Accept','application/json');
        }
        return $next($request);
    }
}
