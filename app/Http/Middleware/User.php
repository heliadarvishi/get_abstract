<?php

namespace App\Http\Middleware;

use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth()->user())
        {
            return response()->json([
                'message' => 'ثبت نام انجام نشده',
                'code' => 403
            ]);
        }
        $user = auth()->user();
        if( $user->type != '1'){
            return response()->json([
                'message' => 'دسترسی غیر مجاز به پنل یوزر',
                'code'    => 403
            ]);
        }
        return $next($request);
    }
}
