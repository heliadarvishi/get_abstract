<?php

namespace App\Http\Resources;
use App\Models\Summary;

use Illuminate\Http\Resources\Json\JsonResource;

class BookMarkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $summary = Summary::find($this->summary_id);

        return[
           'id'       => $summary->id,
           'name'     => $summary->title,
           'cover'    => 'https://cdn.pielem.ir/uploads/images/summary_cover/'.$summary->cover
          ];
    }
}
