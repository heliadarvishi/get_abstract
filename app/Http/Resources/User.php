<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return[
            "id"        => getAuthor($this->user_id)->id,
            "fist_name" => getAuthor($this->user_id)->first_name,
            "last_name" => getAuthor($this->user_id)->last_name,
            "about"     => getAuthor($this->user_id)->about,
            "avatar"    => 'https://cdn.pielem.ir/uploads/images/profile_pic/'.getAuthor($this->user_id)->avatar,
            "book_mark" => bookMarkCalculate($this->user_id,'2'),
            // "rate"      => rateCalculate($this->id)
        ];
    }
}
