<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

// use App\Models\Channel;

class Channel extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(isset($this->parent_id))
        return[
            "id"          => $this->id,
            "title"       => getTitleChannel($this->parent_id),
            "channel_id"  => $this->parent_id,
            "logo"        => getLogoChannel($this->parent_id),
        ];
        else
        return[
            "id"          => $this->id,
            "title"       => $this->title,
            "logo"        => 'https://cdn.pielem.ir/uploads/images/channel_logo/'.$this->logo,
        ];

        // return parent::toArray($request);
    }
}
