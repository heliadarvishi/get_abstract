<?php

namespace App\Http\Resources;
use Verta;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\BookMark;

class Summary extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(auth()->user() != null)
        return[
            "id"               => $this->id,
            "title"            => $this->title,
            "cover"            => 'https://cdn.pielem.ir/uploads/images/summary_cover/'.$this->cover,
            "bookmark"         => bookMarkCalculate($this->id,'1'),
            "rate"             => rateCalculate($this->id,'1')
        ];
        else
        return[
            "id"               => $this->id,
            "title"            => $this->title,
            "cover"            => 'https://cdn.pielem.ir/uploads/images/summary_cover/'.$this->cover,
            "rate"             => rateCalculate($this->id,'1')
        ];

    }
}
