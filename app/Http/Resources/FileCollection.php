<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\File as FileResource;

class FileCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return[
        //     "data" => FileResource::collection($this->collection), 
        //     $this->attributes([
        //         'current_page'    ,
        //         'first_page_url'  ,
        //         'year_publication',
        //         'from'            ,
        //         'last_page'       ,
        //         'last_page_url'   ,
        //         'next_page_url'   ,
        //         'path'            ,
        //         'per_page'        ,
        //         'prev_page_url'   ,
        //         'to'              ,
        //         'total'
        //     ]),
        // ];
    }
    
}
