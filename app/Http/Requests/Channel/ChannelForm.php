<?php

namespace App\Http\Requests\Channel;

use App\Models\Channel;
use Illuminate\Foundation\Http\FormRequest;

class ChannelForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required',
            'logo'   => 'required',
            'logo.*' => 'mimes:jpeg,bmp,png'
        ];
    }

    public function store()
    {
//        return $this->only('logo');
         return Channel::createChannel($this->all());
    }

    public function update($channel)
    {
        $channel = new $channel;
        return $channel->updateChannel($channel,$this->all());
    }
}
