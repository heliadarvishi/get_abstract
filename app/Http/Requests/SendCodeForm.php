<?php

namespace App\Http\Requests;

use App\Models\Sms;
use Illuminate\Foundation\Http\FormRequest;
use Ipecompany\Smsirlaravel\Smsirlaravel;

class SendCodeForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => 'required|max:11'
        ];
    }
        public function store()
    {
        // return Smsirlaravel::getLines();
        $code = random_int(1111,9999);
        // Smsirlaravel::ultraFastSend(['VerificationCode'=>$code],16528,$this->mobile);
        $sms = Sms::firstOrCreate(['mobile'=> $this->mobile]);
        $sms->code = $code;
        $sms->save();
        return $sms ? true : false;
    }
}
