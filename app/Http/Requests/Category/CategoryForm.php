<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Channel;

class CategoryForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_one"  => "required",
            "category_two"  => "required",
            "category_tree" => "required"
        ];
    }

    public function showInfo()
    {
        return Channel::showInfo($this->only('category_one','category_two','category_tree'));
    }
}
