<?php

namespace App\Http\Requests;

use App\Models\Sms;
use App\Models\User;

use Illuminate\Foundation\Http\FormRequest;

class RegisterForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => 'required|max:11',
            'code'   => 'required|max:4'
        ];
    }

    public function check()
    {
        return User::register($this->all());
    //     $sms = Sms::where([
    //         ['mobile',$this->mobile],
    //         ['code',$this->code]
    //     ])->first();
    //     if(is_null($sms))
    //         return ["message" => "the code is invalid!"];
    //     $user = new User;
    //     $user = User::updateOrCreate([
    //         'mobile' => $this->mobile,
    //         'type'   => "1"
    //     ]);
    //     $token = $user->createToken('user_token')->accessToken;
    //     return ["token" => $token];
    }
}
