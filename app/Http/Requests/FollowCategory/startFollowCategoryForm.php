<?php

namespace App\Http\Requests\FollowCategory;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\FollowCategory;

class startFollowCategoryForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category_id_one"  => "required",
            "category_id_two"  => "required",
            "category_id_tree" => "required"
        ];
    }

    public function store()
    {
        return FollowCategory::store($this->all());
    }
}
