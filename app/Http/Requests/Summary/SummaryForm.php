<?php

namespace App\Http\Requests\Summary;

use App\Models\Summary;
use Illuminate\Foundation\Http\FormRequest;

class SummaryForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required',
            'category_id' => 'required',
            'channel_id'  => 'required',
            'file'        => 'mimes:jpeg,bmp,png,doc,mp3,mp4,dox,pdf,rar',
            'text'        => 'min : 100'
        ];
    }

    public function store()
    {
        $files = app('request')->post('form_field_name');
        return $files; // array
//        return Summary::store($this->all());
    }

    public function update($summary)
    {
        $summary = new Summary;
        return $summary->updateSummary($summary,$this->all());
    }
}
