<?php

namespace App\Http\Controllers\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\Category\CategoryForm;
class CategoryController extends Controller
{
    public function show()
    {
        $category = new Category;
        $categories = $category->paginate(5);
        return response()->json($categories,200);
    }

    public function showInfo(CategoryForm $form)
    {
        return response()->json($form->showInfo(),200);
    }

}
