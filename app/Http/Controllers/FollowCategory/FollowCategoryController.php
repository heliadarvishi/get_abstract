<?php

namespace App\Http\Controllers\FollowCategory;
use App\Http\Resources\HomeCollection;
use App\Http\Resources\RecommendedForYouCollection;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FollowCategory\startFollowCategoryForm;

class FollowCategoryController extends Controller
{
    public function store(startFollowCategoryForm $form)
    {
        return response()->json($form->store(),200);
    }

    public function startStoreAndShow(startFollowCategoryForm $form)
    {
        // $user = auth()->user();
        // $form->store();
        // $response = new HomeCollection($user);
        // return response()->json($response,200);
    }

    public function show( Request $request)
    {
        $user = auth()->user();
        $response = new HomeCollection($user);
        return response()->json($response,200);
    }

    public function feachuredChannel()
    {
        $user = auth()->user();
        return response()->json(new SummaryCollection($user->followCategory()->channel($user->id)));
    }
}
