<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterForm;
use App\Http\Requests\SendCodeForm;
use Illuminate\Http\Request;

class authenticationController extends Controller
{
    public function sendCode(SendCodeForm $form)
    {
        return response()->json($form->store(),200);
    }
    
    public function register(RegisterForm $request)
    {
        return response()->json($request->check(),201);
    }
}
