<?php

namespace App\Http\Controllers\Like;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Like;
use App\Models\Summary;

class LikeController extends Controller
{
    public function like(Summary $summary)
    {
        return response()->json(Like::likeAndDislike($summary),200);
    }

    public function countSummaryLike(Summary $summary)
    {
        return response()->json(Like::countLike($summary),200);
    }
}
