<?php

namespace App\Http\Controllers\Channel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FollowCategory;
use App\Http\Resources\ChannelCollection;
use App\Models\Channel;
use App\Models\Category;
use App\Http\Requests\PaginateForm;

class ChannelController extends Controller
{
    public function featuredChannels($paginate)
    {
        // return response()->json(FollowCategory::featuredChannels(),200);
        return response()->json(new ChannelCollection(FollowCategory::featuredChannels()->paginate($paginate)),200);
    }

    public function showAll($paginate)
    {
        $channel = new Channel;
        $channels = $channel->paginate($paginate);
        return response()->json($channels,200);
    }

    public function searchByCategory(Category $category,$paginate)
    {
        $channels = Channel::where('category_id',$category->id)->paginate($paginate);
        return response()->json(new ChannelCollection($channels,200));
    }


    //for public data

    public function getPublicLatestChannels($paginate)
    {
        return response()->json(new ChannelCollection(Channel::getPaublicChannel($paginate),200));
    }
}
