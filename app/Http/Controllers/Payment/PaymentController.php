<?php

namespace App\Http\Controllers\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Summary;

class PaymentController extends Controller
{
    public function store(Request $request,Summary $summary)
    {
        $user = auth()->user();
        // $callback = "http://api.pielem.ir/summary/callback/";
        $callback_parametr = [
            'X_API_TOKEN' => env('X_API_TOKEN'),
            'authorization' => $request->header('authorization')
        ];
        $callback = route('user.callback',$callback_parametr);
        $zibal = zibalRequest($user->mobile,$callback,$summary);

        return response()->json($zibal,200);
    }

    public function callback(Request $request)
    {
        // return $request;
        $user = auth()->user();
        $verify_data = [
            'success' => $request['success'],
            'status'  => $request['status'],
            'trackId' => $request['trackId']
        ];

        $callback = callback($verify_data);
        // return $callback;
        return view('User.callback',compact('user','verify_data','callback'));
    }
}
