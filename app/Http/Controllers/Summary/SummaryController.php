<?php

namespace App\Http\Controllers\Summary;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FollowCategory;
use App\Http\Resources\SummaryCollection;
use App\Http\Resources\RateCollection;

use App\Http\Resources\Summary as SummaryResource;
use App\Models\Summary;
use App\Models\Channel;

class SummaryController extends Controller
{
    public function show(Summary $summary)
    {
        return response()->json(Summary::fullSummary($summary),200);
    }

    public function recommendedForYou($paginate)
    {
        return response()->json(new SummaryCollection(FollowCategory::recommended()->paginate($paginate)),200);
        // return response()->json(["data" => new SummaryCollection(FollowCategory::recommended())]);
    }

    public function getChannelSummaries(Channel $channel,$paginate)
    {
        return response()->json(["data" => new SummaryCollection(Summary::where([
            ['channel_id',$channel->id],
            ['status','1']
        ])->paginate($paginate))]);
    }

    public function getLatestSummaries($paginate)
    {
        return response()->json(new SummaryCollection(FollowCategory::recommended()->latest()->paginate($paginate)),200);
    }

    public function getPopularSummaries($paginate)
    {
        return response()->json(new RateCollection(FollowCategory::getPopularSummaries()->paginate($paginate)),200);
    }

    //for public data

    public function getPublicLatestSummaries($paginate)
    {
        return response()->json(new SummaryCollection(Summary::getPaublicSummary($paginate),200));
    }

    public function publicShow(Summary $summary)
    {
        return response()->json(Summary::fullSummary($summary),200);
    }

    public function getpublicChannelSummaries(Channel $channel,$paginate)
    {
        return response()->json(["data" => new SummaryCollection(Summary::where([
            ['channel_id',$channel->id],
            ['status','1']
        ])->paginate($paginate))]);
    }
}
 