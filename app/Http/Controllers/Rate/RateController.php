<?php

namespace App\Http\Controllers\Rate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rate;
use App\Models\Summary;
use App\Models\User;

class RateController extends Controller
{
    public function summaryStore(Summary $summary,$rank)
    {
        return response()->json(Rate::store($summary,1,$rank),200);
    }

    public function authorStore(User $user,$rank)
    {
        return response()->json(Rate::store($user,2,$rank),200);
    }
}
