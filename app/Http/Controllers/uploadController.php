<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileFormRequest;
use Illuminate\Http\Request;
use File;

class uploadController extends Controller
{
    public $files = [];
    public function get(FileFormRequest $request)
    {
        $files_dir = storage_path('app').'/spreadsheets';
        foreach (File::allFiles($files_dir) as $file) {
            $file = $file->getBasename();
            $this->files[$file] = $file;
        }

//        return view('main', array(
//            'title' => '',
//            'file_names' => $this->files,
//            'file_name' => '',
//            'sheet_name' => '',
//            'encoding' => 'UTF-8'
//        ));
    }
}
