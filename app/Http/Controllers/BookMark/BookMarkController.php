<?php

namespace App\Http\Controllers\BookMark;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BookMark;
use App\Models\Summary;
use App\Http\Requests\BookMark\BookMarkForm;
use App\Http\Resources\BookMarkCollection;

class BookMarkController extends Controller
{
    public function bookMark(Summary $summary)
    {
        return response()->json(BookMark::bookmarkAndUnBookmark($summary),200);
    }

    public function showSummaries($paginate)
    {
        $bookmarks = BookMark::where("type",'1')->paginate($paginate);
        return response()->json($bookmarks,200);
    }
}
