<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\Profile\FullNameForm;
use App\Http\Requests\Profile\UserNameForm;
use App\Http\Requests\Profile\AvatarForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ProfileController extends Controller
{
    public function updateUserName(UserNameForm $form)
    {
        return response()->json($form->update(),200);
    }

    public function updateFullName(FullNameForm $form)
    {
        return response()->json($form->update(),200);
    }

    public function updateAvatar(AvatarForm $form)
    {
        return $form->update();
    }
}
