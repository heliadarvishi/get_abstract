<?php

namespace App\Http\Controllers\User;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Models\FollowCategory;

class UserController extends Controller
{
    public function showAuthorByFollowCategory($paginate)
    {
        return response()->json(new UserCollection(FollowCategory::getAuthors()->paginate($paginate)),200);
    }

    public function showAllAuthor($paginate)
    {
        $users = User::where("type","3")->orwhere("type","4")->paginate($paginate);
        return response()->json($users,200);
    }
}
