<?php

namespace App\Http\Controllers\Follow;
use App\Http\Requests\Follow\StartFollowForm;
use App\Models\Follow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Summary;
use App\Models\Channel;
use App\Http\Resources\HomeCollection;


class FollowController extends Controller
{
    public function channelStore(Channel $channel)
    {
        return response()->json(Follow::store($channel,'2'),200);
    }

    public function authorStore(User $user)
    {
        return response()->json(Follow::store($user,'1'),200);
    }
}
