
<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8" />
      <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('assets/img/apple-icon.png')}}">
      <link rel="icon" type="image/png" href="{{URL::asset('assets/img/favicon.png')}}">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <title>
      {{env('APP_NAME')}} | نتیجه پرداخت
      </title>
      <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
      <!--     Fonts and icons     -->
      <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
      <!-- CSS Files -->
      <link href="{{URL::asset('assets/css/material-kit.css?v=2.0.6')}}" rel="stylesheet" />
      <!-- CSS Just for demo purpose, don't include it in your project -->
      <link href="{{URL::asset('assets/demo/demo.css')}}" rel="stylesheet" />
      <link href="{{URL::asset('assets/css/style.css')}}" rel='stylesheet' type='text/css' />	
    </head>
    
    <body class="index-page sidebar-collapse" style="font-family: 'Yekan','Roboto-Regular';">

      <div class="page-header header-filter clear-filter purple-filter" data-parallax="true" style="background-image: url('./assets/img/bg20.jpg');">
        <div class="container">
          <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
              <div class="brand">
                @if(Session::has('error'))<div class="alert alert-danger text-center">{{Session::get('error')}}</div>@endif
                @if(Session::has('message'))<div class="alert alert-success text-center">{{Session::get('message')}}</div>@endif
        
                <h4 style="font-family: 'Yekan';"> <i class="material-icons">mood</i> وضعیت : @if($callback->message == "success") تراکنش با موفقیت انجام شد @endif </h4>
                <h4 style="font-family: 'Yekan';"> قیمت : {{$callback->amount}} تومان </h4>
                <h4 style="font-family: 'Yekan';"> کد رهگیری :  </h4>
                <button class="gotoapp">بازگشت به اپلیکیشن</button>
                <!-- <a href="">بازگشت به اپلیکیشن</a> -->
              </div>
            </div>
          </div>
        </div>
      </div>

        <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
        <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
        <script src="../assets/js/plugins/moment.min.js"></script>
        <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
        <script src="../assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
        <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
        <script src="../assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
        <script src="../assets/js/material-kit.js?v=2.0.6" type="text/javascript"></script>
    </body>
</html>




