<?php
Route::group(['middleware' => 'app_key'], function () {
    Route::post('sendCode', 'authenticationController@sendCode')->name('send-code');
    Route::post('register', ['as' => 'register', 'uses' => 'authenticationController@register']);

        //public data
        //summary
        Route::namespace('Summary')->group(function () {
            Route::get('latest-summaries/{paginate}','SummaryController@getPublicLatestSummaries');
            Route::get('show/{summary}','SummaryController@publicShow');
            Route::get('channel-summaries/{channel}/{paginate}','SummaryController@getpublicChannelSummaries');
        });
        
        // Category
        Route::namespace('Category')->group(function () {
            Route::get('show','CategoryController@show');
        });

        Route::namespace('Channel')->group(function () {
            Route::get('latest-channels/{paginate}','ChannelController@getPublicLatestChannels');
            Route::get('search-channel-by-categories/{category}/{channel}','ChannelController@searchByCategory');         
        });

    Route::middleware(['auth:api'])->group(function (){
        Route::middleware(['is_user'])->group(function () {

            // User
            Route::namespace('User')->prefix('user')->group(function () {
                Route::put('user-name','profileController@updateUserName')->name('user-name');
                Route::put('full-name','ProfileController@updateFullName')->name('full-name');
                Route::post('avatar','ProfileController@updateAvatar')->name('avatar');
                //show home info(author) by follow info
                Route::get('show/authors/{paginate}','UserController@showAuthorByFollowCategory');
                Route::get('show-all/{paginate}','UserController@showAllAuthor');
            });

            // Category
            Route::namespace('Category')->prefix('category')->group(function () {
                Route::get('show','CategoryController@show');
            });

            // Follow
            Route::namespace('Follow')->prefix('follow')->group(function () {
                Route::post('/channel/{channel}','FollowController@channelStore');
                Route::post('/user/{user}','FollowController@authorStore');
            });

            // Like
            Route::namespace('Like')->prefix('like')->group(function () {
                Route::post('/{summary}','LikeController@like');
                Route::get('/show/{summary}','LikeController@countSummaryLike');
            });

            // BookMark
            Route::namespace('BookMark')->prefix('book-mark')->group(function () {
                Route::get('/summaries/show/{paginate}','BookMarkController@showSummaries');
                Route::post('/{summary}','BookMarkController@bookMark');
            });

            // Summary
            Route::namespace('Summary')->prefix('summary')->group(function () {
                Route::get('recommended-for-you/{paginate}','SummaryController@recommendedForYou');
                Route::get('show/{summary}','SummaryController@show');
                Route::get('channel-summaries/{channel}/{paginate}','SummaryController@getChannelSummaries');
                Route::get('latest-summaries/{paginate}','SummaryController@getLatestSummaries');
                Route::get('popular-summaries/{paginate}','SummaryController@getPopularSummaries');
                // Route::get('search-by-summaries','ChannelController@searchByCategory');
            });

            // Channel
            Route::namespace('Channel')->prefix('channel')->group(function () {
                Route::get('show-all/{paginate}','ChannelController@showAll');
                Route::get('featured-channels/{paginate}','ChannelController@featuredChannels');
                Route::get('search-by-categories/{category}/{paginate}','ChannelController@searchByCategory');
            });

            // Follow Category
            Route::namespace('FollowCategory')->prefix('follow-category')->group(function () {
                //in 3ta category migire save mikone
                Route::post('store','FollowCategoryController@store')->name('store');
                
                //feachured channel
                Route::get('feachured-channel','FollowCategoryController@feachuredChannel')->name('feachured-channel');
                
                //show summaries by follow category info
                Route::get('recommended-show','FollowCategoryController@show')->name('recommended-show');
            });

            Route::namespace('Rate')->prefix('rate')->group(function () {
                Route::post('/summary/{summary}/{rank}','RateController@summaryStore');
                Route::post('/author/{user}/{rank}','RateController@authorStore');
            });

            Route::namespace('Payment')->prefix('payment')->group(function () {
                Route::get('store/{summary}','PaymentController@store');
                Route::get('/callback', ["as" => "user.callback", "uses" => "PaymentController@callback"]);
            });

            Route::namespace('Search')->prefix('search')->group(function () {

            });
        });
    });
});

